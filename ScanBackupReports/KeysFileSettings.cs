﻿using System;

namespace ScanBackupReports
{
    /// <summary>
    /// Содержит ключи настроек для файла Settings.ini
    /// </summary>
    struct KeysFileSettings
    {
        /// <summary>
        /// Директория для поиска файлов отчёта
        /// </summary>
        public const string KEY_PATH_SCANS_FILES = "searchFileReport_directory";

        /// <summary>
        /// Маска имени файла отчета для фильтрации документов при поиске
        /// </summary>
        public const string KEY_TEMPLATE_SCANS_FILES = "searchFileReport_template";

        /// <summary>
        /// Номер строки для проверки резервных копий БД
        /// </summary>
        public const string KEY_BACKUP_SCANS_FILES = "contentFileReport_backup";

        /// <summary>
        /// Номер строки для проверки целостности БД
        /// </summary>
        public const string KEY_INTEGRITY_SCANS_FILES = "contentFileReport_integrity";
    }
}
