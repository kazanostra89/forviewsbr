﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ScanBackupReports
{
    static class Helper
    {
        private static bool rescan;
        private static object hubForReports;
        private static object hubForFileReport;
        private static object hubForReportsDataBase;

        static Helper()
        {
            rescan = false;
            hubForReports = null;
            hubForReports = null;
        }


        /// <summary>
        /// Служит для передачи коллекции FileReport
        /// </summary>
        public static object HubForReports
        {
            get { return hubForReports; }

            set { if (value is List<FileReport>) hubForReports = value; }
        }

        /// <summary>
        /// Служит для передачи экземпляра FileReport
        /// </summary>
        public static object HubForFileReport
        {
            get { return hubForFileReport; }

            set { if (value is FileReport) hubForFileReport = value; }
        }

        public static object HubForReportsDataBase
        {
            get { return hubForReportsDataBase; }

            set { if (value is List<FileReport>) hubForReportsDataBase = value; }
        }

        /// <summary>
        /// Позволяет выполнить повторное сканирование после изменения настроек
        /// </summary>
        public static bool Rescan
        {
            get { return rescan; }

            set { rescan = value; }
        }

        public static void CallMessageForNotifyIcon(string title, string message, ToolTipIcon icon)
        {
            if (NotifyIconMessages != null)
            {
                NotifyIconMessages(title, message, icon);
            }
        }

        public delegate void NotifyIconHandler(string title, string message, ToolTipIcon icon);

        public static event NotifyIconHandler NotifyIconMessages;

    }
}
