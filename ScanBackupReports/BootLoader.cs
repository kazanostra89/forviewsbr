﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace ScanBackupReports
{
    static class BootLoader
    {
        private static Dictionary<string, string> settings;

        static BootLoader()
        {
            settings = new Dictionary<string, string>();
            PathFileSettings = Directory.GetCurrentDirectory() + "\\Settings.ini";
            PathFileDbSBR = Directory.GetCurrentDirectory() + "\\SBR.db";
            StatusFileSettings = StatusFileSettings.NoFile;
            
            if (GetStatusExistsFile(PathFileSettings))
            {
                LoadingSettingsFromFile();

                StatusFileSettings = StatusFileSettings.IncorrectCheckTheContents;

                if (CheckingForKeys() && CheckingForKeyValues())
                {
                    StatusFileSettings = StatusFileSettings.CorrectDirectoryIsReadyForScanning;
                }
            }
        }

        /// <summary>
        /// Выполняет чтение файла настройки Settings.ini и заполняет словарь для хранения настроек программы
        /// </summary>
        private static void LoadingSettingsFromFile()
        {
            try
            {
                using (StreamReader readSettings = new StreamReader(PathFileSettings))
                {
                    while (!readSettings.EndOfStream)
                    {
                        string readLine = readSettings.ReadLine();

                        if (readLine.StartsWith(";") || !MatchesTheRule(readLine))
                        {
                            continue;
                        }

                        string[] readLines = readLine.Split('=');

                        if (settings.Keys.Contains(readLines[0]) || readLines[1] == String.Empty)
                        {
                            continue;
                        }

                        settings.Add(readLines[0], readLines[1]);
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Чтение файла настроек Settings.ini!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            bool MatchesTheRule(string checkReadLine)
            {
                int count = 0;

                foreach (char item in checkReadLine)
                {
                    if (item == '=')
                    {
                        count++;
                    }
                }

                return count == 1 ? true : false;
            }
        }

        /// <summary>
        /// Выполняет создание или обновление файла настроек Settings.ini
        /// </summary>
        /// <param name="directoryScans"></param>
        public static void CreateAndUpdateFileSettings(string directoryScans, string template, string backupRow, string integrityRow)
        {
            string contents =   KeysFileSettings.KEY_PATH_SCANS_FILES + "=" + directoryScans + "\r\n"
                                + KeysFileSettings.KEY_TEMPLATE_SCANS_FILES + "=" + template + "\r\n"
                                + KeysFileSettings.KEY_BACKUP_SCANS_FILES + "=" + backupRow + "\r\n"
                                + KeysFileSettings.KEY_INTEGRITY_SCANS_FILES + "=" + integrityRow;

            File.WriteAllText(PathFileSettings, contents, Encoding.UTF8);

            if (directoryScans == String.Empty)
            {
                LoadingSettingsFromFile();

                StatusFileSettings = StatusFileSettings.CorrectNoDirectoryForScanning;
            }
            else
            {
                if (StatusFileSettings == StatusFileSettings.CorrectDirectoryIsReadyForScanning || StatusFileSettings == StatusFileSettings.CorrectNoDirectoryForScanning)
                {
                    settings[KeysFileSettings.KEY_PATH_SCANS_FILES] = directoryScans;
                    settings[KeysFileSettings.KEY_TEMPLATE_SCANS_FILES] = template;
                    settings[KeysFileSettings.KEY_BACKUP_SCANS_FILES] = backupRow;
                    settings[KeysFileSettings.KEY_INTEGRITY_SCANS_FILES] = integrityRow;
                }
                else if (StatusFileSettings == StatusFileSettings.IncorrectCheckTheContents || StatusFileSettings == StatusFileSettings.NoFile)
                {
                    if (settings.Count > 0)
                    {
                        settings.Clear();
                    }

                    LoadingSettingsFromFile();
                }

                StatusFileSettings = StatusFileSettings.CorrectDirectoryIsReadyForScanning;
            }
        }

        /// <summary>
        /// Проверяет наличие существования файла
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool GetStatusExistsFile(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Проверяет наличие существования директории
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool GetStatusExistsDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Возвращает значение типа string по ключу "Словаря"
        /// </summary>
        /// <param name="key">Имя ключа "Словаря"</param>
        /// <returns>Возвращает пустую строку, если нет ключа</returns>
        public static string GetStringValueOfSettingsFile(string key)
        {
            if (settings.Keys.Contains(key))
            {
                return settings[key];
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Возвращает значение типа int по ключу "Словаря"
        /// </summary>
        /// <param name="key">Имя ключа "Словаря"</param>
        /// <returns>Возвращает значение 0, если нет ключа или его значения</returns>
        public static int GetIntValueOfSettingsFile(string key)
        {
            if (settings.Keys.Contains(key) && CheckingTheNumber(settings[key]))
            {
                return Math.Abs(Convert.ToInt32(settings[key]));
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Проверка наличия необходимых ключей в словаре, после чтения файла настройки Settings.ini
        /// </summary>
        /// <returns></returns>
        private static bool CheckingForKeys()
        {
            Dictionary<string, string>.KeyCollection keys = settings.Keys;

            return  keys.Contains(KeysFileSettings.KEY_PATH_SCANS_FILES)
                    && keys.Contains(KeysFileSettings.KEY_TEMPLATE_SCANS_FILES)
                    && keys.Contains(KeysFileSettings.KEY_BACKUP_SCANS_FILES)
                    && keys.Contains(KeysFileSettings.KEY_INTEGRITY_SCANS_FILES);
        }

        /// <summary>
        /// Проверка значений соответствующих ключей на достоверность информации,
        /// необходимой для работы программы.
        /// </summary>
        /// <returns></returns>
        private static bool CheckingForKeyValues()
        {
            return  GetStatusExistsDirectory(settings[KeysFileSettings.KEY_PATH_SCANS_FILES])
                    && settings[KeysFileSettings.KEY_TEMPLATE_SCANS_FILES] != String.Empty
                    && CheckingTheNumber(settings[KeysFileSettings.KEY_BACKUP_SCANS_FILES])
                    && CheckingTheNumber(settings[KeysFileSettings.KEY_INTEGRITY_SCANS_FILES]);
        }

        /// <summary>
        /// Проверяет возможность преобразования строкового значения в числовое
        /// </summary>
        /// <param name="keyValue">
        /// Строковое значение для преобразования
        /// </param>
        /// <returns></returns>
        private static bool CheckingTheNumber(string keyValue)
        {
            int number;
            bool check;

            check = int.TryParse(keyValue, out number);

            return (check == true && !keyValue.StartsWith("0") && number >= 0) ? true : false;
        }

        /// <summary>
        /// Возвращает статус файла настройки Settings.ini для работы с программой
        /// </summary>
        /// <returns></returns>
        public static string GetCorrectValueStatusFileSetting()
        {
            switch (StatusFileSettings)
            {
                case StatusFileSettings.IncorrectCheckTheContents:
                    return "Файл присутствует. Некорректное содержимое файла";
                case StatusFileSettings.CorrectDirectoryIsReadyForScanning:
                    return "Всё готово для сканирования";
                case StatusFileSettings.CorrectNoDirectoryForScanning:
                    return "Файл создан. Значения настроек пусты";
                default:
                    return "Файл настроек отсутствует";
            }
        }

        /// <summary>
        /// Возвращает полный путь к файлу настроек Settings.ini
        /// </summary>
        public static string PathFileSettings { get; private set; }

        /// <summary>
        /// Возвращает полный путь к файлу базы данных SBR.db
        /// </summary>
        public static string PathFileDbSBR { get; private set; }

        /// <summary>
        /// Возвращает enum статус файла настройки Settings.ini для работы с программой
        /// </summary>
        public static StatusFileSettings StatusFileSettings { get; private set; }



    }
}
