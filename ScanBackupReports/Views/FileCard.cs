﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScanBackupReports.Views
{
    public partial class FileCard : Form
    {
        private FileReport fileReport;

        public FileCard()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            fileReport = Helper.HubForFileReport as FileReport;
        }

        private void FileCard_Load(object sender, EventArgs e)
        {
            if (fileReport != null)
            {
                this.Text = fileReport.Name;
                textBoxFileCardName.Text = fileReport.Name;
                textBoxFileCardDirectoryName.Text = fileReport.DirectoryName;
                textBoxFileCardDateCreation.Text = fileReport.DateCreation.ToString("dd.MM.yyyy HH:mm:ss");
                textBoxFileCardDateModified.Text = fileReport.DateModified.ToString("dd.MM.yyyy HH:mm:ss");
                textBoxFileCardStatus.Text = fileReport.BackupIsCorrect ? "Успешно" : "Неуспешно";
                textBoxFileCardSize.Text = fileReport.SizeKbyte.ToString("0.00000");
                textBoxHashMD5.Text = fileReport.MD5;
            }
        }
    }
}
