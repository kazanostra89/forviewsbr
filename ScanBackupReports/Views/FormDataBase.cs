﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScanBackupReports.Views
{
    public partial class FormDataBase : Form
    {
        private DbManager db;

        public FormDataBase()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            db = null;
            buttonUploadToTheDatabase.Enabled = false;
        }

        #region MyMethods
        private void DeletingDataBaseFile(string message, string author)
        {
            MessageBox.Show(message, author, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            if (File.Exists(BootLoader.PathFileDbSBR))
            {
                try
                {
                    File.Delete(BootLoader.PathFileDbSBR);
                }
                catch
                {
                    MessageBox.Show("Не удалось выполнить удаление файла " + BootLoader.PathFileDbSBR + " программным способом.\nУдалите данный файл вручную!", "Удаление файла БД SQLite", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            this.Close();
        }

        private List<FileReport> LoadingDataFromTheDatabase()
        {
            return db.TableFileReports.GetAllFileReports();
        }

        private void CheckingFileDbSQLite()
        {
            if (File.Exists(BootLoader.PathFileDbSBR))
            {
                SuccessfulMessages(true, "Всё готово для работы с локальной БД!");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("Файл локальной БД не найден!\nСоздать файл \"SBR.db\" для продолжения работы?", "Локальная БД", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                bool resultCreateFile = db.CreateFileDbSQLite();

                if (resultCreateFile)
                {
                    SuccessfulMessages(true, "Файл \"SBR.db\" успешно создан:\n" + BootLoader.PathFileDbSBR + "\nВсё готово для работы с локальной БД!");
                }
                else
                {
                    DeletingDataBaseFile("В локальной БД отсутсвует таблица Reports, повторите операцию!", "Локальная БД");
                }
            }
            else
            {
                SuccessfulMessages(false, "Для продолжения работы,\nнеобходимо создать файл \"SBR.db\" базы данных!\n"
                                          + "Форма была принудительна закрыта!");
                this.Close();
            }

            void SuccessfulMessages(bool toUseStatusLabel, string message)
            {
                Helper.CallMessageForNotifyIcon("Локальная БД", message, ToolTipIcon.Info);

                if (toUseStatusLabel) statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Всё готово для работы с локальной БД";
            }
        }
        #endregion

        private void FormDataBase_Load(object sender, EventArgs e)
        {
            try
            {
                db = DbManager.GetDbManager();

                CheckingFileDbSQLite();
            }
            catch (Exception error)
            {
                DeletingDataBaseFile(error.Message, "Локальная БД");
            }
        }

        private async void buttonLoadDataBase_Click(object sender, EventArgs e)
        {
            listBoxFromDataBase.DataSource = null;

            try
            {
                db.ConnectionOpen();
                Helper.CallMessageForNotifyIcon("Загрузка из БД", "Начато чтение данных из локальной БД...", ToolTipIcon.Info);

                List<FileReport> reports = await Task.Run(new Func<List<FileReport>>(LoadingDataFromTheDatabase));
               
                listBoxFromDataBase.DataSource = reports;

                if (reports.Count == 0)
                {
                    Helper.CallMessageForNotifyIcon("Загрузка из БД", "Отчетов в БД не найдено!", ToolTipIcon.Info);
                    statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": В локальной БД отсутствуют данные!";
                    return;
                }

                Helper.CallMessageForNotifyIcon("Загрузка из БД", "Загрузка успешно завершена!\nВсего файлов загружено: " + reports.Count, ToolTipIcon.Info);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + $": Успешно загружено {reports.Count} файла(ов) из локальной БД!";
            }
            catch (Exception error)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + " (Ошибка): Список файлов в объекте был очищен! Повторите операцию";
                MessageBox.Show(error.Message, "SQLite DB", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
            }
        }

        private void buttonLoadScanned_Click(object sender, EventArgs e)
        {
            if (Helper.HubForReports is List<FileReport>)
            {
                listBoxFromScanned.DataSource = Helper.HubForReports;

                buttonLoadScanned.Enabled = false;

                Helper.CallMessageForNotifyIcon("Загрузка сканированных отчетов", "Сканированные отчеты успешно загружены!", ToolTipIcon.Info);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + $": Успешно загружено {((List<FileReport>)Helper.HubForReports).Count} сканированных файла(ов)!";
            }
            else
            {
                Helper.CallMessageForNotifyIcon("Загрузка сканированных отчетов", "Сканирование не производилось или файлы отчеты в сканированной директории не найдены!", ToolTipIcon.Info);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Сканирование не производилось или файлы отчетов при сканировании не были найдены!";
            }
        }

        private async void buttonSearchUnique_Click(object sender, EventArgs e)
        {
            if (listBoxFromScanned.DataSource == null)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Сканирование не производилось или файлы отчетов при сканировании не были найдены!";
                return;
            }

            if (listBoxFromDataBase.DataSource == null)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Необходимо выполнить чтение данных из локальной БД!";
                return;
            }

            buttonSearchUnique.Enabled = false;

            List<FileReport> dataUnique = new List<FileReport>();
            List<FileReport> dataScanned = (List<FileReport>)listBoxFromScanned.DataSource;
            List<FileReport> dataBase = (List<FileReport>)listBoxFromDataBase.DataSource;

            await Task.Run(new Action(() =>
            {
                foreach (FileReport dS in dataScanned)
                {
                    if (!(dataBase.Exists(dB => dB.MD5 == dS.MD5) || dataBase.Exists(dB => dB.Name == dS.Name) || dataBase.Exists(dB => dB.FullName == dS.FullName)))
                    {
                        dataUnique.Add(dS);
                    }
                }
            }));

            if (dataUnique.Count == 0)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Новых данных для записи в локальную БД не найдено!";
                return;
            }

            buttonUploadToTheDatabase.Enabled = true;
            listBoxUnique.DataSource = dataUnique;

            statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Данные готовы для записи в локальную БД!";
        }

        private async void buttonUploadToTheDatabase_Click(object sender, EventArgs e)
        {
            try
            {
                db.ConnectionOpen();
                
                List<FileReport> dataToUpload = (List<FileReport>)listBoxUnique.DataSource;

                bool result = await Task.Run(new Func<bool>(() => db.TableFileReports.InsertFileReports(dataToUpload)));

                if (result)
                {
                    statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Новые данные добавленны в локальную БД!";

                    listBoxUnique.DataSource = null;
                    buttonUploadToTheDatabase.Enabled = false;

                    listBoxFromDataBase.DataSource = null;
                    listBoxFromDataBase.DataSource = await Task.Run(new Func<List<FileReport>>(LoadingDataFromTheDatabase));
                }
            }
            catch (Exception error)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + " (Ошибка): Поизошла ошибка при сохранении данных в локальную БД!";
                MessageBox.Show(error.Message, "SQLite DB", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
            }
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            if (sender is ListBox)
            {
                ListBox listBox = (ListBox)sender;

                if (listBox.DataSource != null && listBox.Items.Count != 0)
                {
                    Helper.HubForReportsDataBase = listBox.DataSource;

                    using (FormTheReportViewerDataBase form = new FormTheReportViewerDataBase())
                    {
                        form.ShowDialog();
                    }
                }
            }
        }

        private async void buttonTruncateTableReports_Click(object sender, EventArgs e)
        {
            if (!File.Exists(BootLoader.PathFileDbSBR))
            {
                Helper.CallMessageForNotifyIcon("Очистка локальной БД", "Отсутствует файл локальной БД!", ToolTipIcon.Warning);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + " (Ошибка): Отсутствует файл локальной БД!";
                return;
            }

            buttonTruncateTableReports.Enabled = false;

            try
            {
                db.ConnectionOpen();

                await Task.Run(new Action(db.TableFileReports.TruncateTableReports));

                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Полная очистка локальной БД успешно завершена!";
                MessageBox.Show("Полная очистка локальной БД успешно завершена!", "SQLite DB", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (!buttonSearchUnique.Enabled) buttonSearchUnique.Enabled = true;

                listBoxFromDataBase.DataSource = null;
                listBoxFromDataBase.DataSource = await Task.Run(new Func<List<FileReport>>(LoadingDataFromTheDatabase));
            }
            catch (Exception error)
            {
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + " (Ошибка): Поизошла ошибка при удалении данных из локальной БД!";
                MessageBox.Show(error.Message, "SQLite DB", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                db.ConnectionClose();
                if (!buttonTruncateTableReports.Enabled) buttonTruncateTableReports.Enabled = true;
            }
        }

        private async void buttonSaveReportsToFolder_Click(object sender, EventArgs e)
        {
            if (listBoxFromDataBase.DataSource == null)
            {
                Helper.CallMessageForNotifyIcon("Сохранение файлов на диск", "Необходимо выполнить чтение данных из локальной БД!", ToolTipIcon.Warning);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Необходимо выполнить чтение данных из локальной БД!";
                return;
            }

            if (listBoxFromDataBase.Items.Count == 0)
            {
                Helper.CallMessageForNotifyIcon("Сохранение файлов на диск", "В локальной БД отсутствуют файлы для сохранения!", ToolTipIcon.Warning);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": В локальной БД отсутствуют файлы для сохранения!";
                return;
            }

            DialogResult dialogResult = folderToSaveReports.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                buttonSaveReportsToFolder.Enabled = false;

                List<FileReport> reports = (List<FileReport>)listBoxFromDataBase.DataSource;

                try
                {
                    await SaveReportsToFolderAsync(reports, folderToSaveReports.SelectedPath + "\\");

                    Helper.CallMessageForNotifyIcon("Сохранение файлов на диск", "Файлы сохранены в каталог:\n" + folderToSaveReports.SelectedPath, ToolTipIcon.Info);
                    statusLabelFormDataBase.Text = DateTime.Now.ToString() + " : Операция сохранения завершилась успешно!";
                }
                catch (Exception error)
                {
                    statusLabelFormDataBase.Text = DateTime.Now.ToString() + " (Ошибка): Поизошла ошибка при сохранении данных из локальной БД!";
                    MessageBox.Show(error.Message, "Сохранение файлов на диск", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                Helper.CallMessageForNotifyIcon("Сохранение файлов на диск", "Операция сохранения файлов была прервана!", ToolTipIcon.Warning);
                statusLabelFormDataBase.Text = DateTime.Now.ToString() + ": Операция сохранения файлов из локальной БД была прервана!";
            }

            buttonSaveReportsToFolder.Enabled = true;
        }

        private async Task SaveReportsToFolderAsync(List<FileReport> reports, string folder)
        {
            await Task.Run(() =>
            {
                foreach (FileReport report in reports)
                {
                    using (FileStream stream = new FileStream(folder + report.Name, FileMode.Create, FileAccess.Write))
                    {
                        stream.Write(report.Content, 0, report.Content.Length);
                    }
                }
            });
        }


    }
}
