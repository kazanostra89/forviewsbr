﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace ScanBackupReports.Views
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            StatusUpdateOnTheForm();

            SettingFieldValuesFromTheSettingsFile();
        }

        private void buttonOpenFileSettings_Click(object sender, EventArgs e)
        {
            if (BootLoader.StatusFileSettings == StatusFileSettings.NoFile)
            {
                toolStripStatusLabelFormSettings.Text = "Отсутствует файл настроек!";
                return;
            }

            FileInfo fileInfo = null;

            try
            {
                fileInfo = new FileInfo(BootLoader.PathFileSettings);
                fileInfo.Attributes = FileAttributes.ReadOnly;

                Process notepad = Process.Start("notepad.exe", BootLoader.PathFileSettings);
                notepad.WaitForExit();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (fileInfo != null && fileInfo.Attributes != FileAttributes.Archive)
                {
                    fileInfo.Attributes = FileAttributes.Archive;
                }
            }
        }

        private void buttonSelectedDirectory_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialogFormSettings.ShowDialog();

            if (result == DialogResult.OK)
            {
                labelChoisePathSettings.Text = "Каталог сканирования:\n" + folderBrowserDialogFormSettings.SelectedPath;

                toolStripStatusLabelFormSettings.Text = "Необходимо выполнить сохранение настроек";
            }
        }

        private void StatusUpdateOnTheForm()
        {
            toolStripStatusLabelFormSettings.Text = "Settings.ini:  " + BootLoader.GetCorrectValueStatusFileSetting();
        }

        private void SettingFieldValuesFromTheSettingsFile()
        {
            if (BootLoader.StatusFileSettings == StatusFileSettings.CorrectDirectoryIsReadyForScanning)
            {
                string directoryScan = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_PATH_SCANS_FILES);

                labelChoisePathSettings.Text = "Каталог сканирования:\n" + directoryScan;

                folderBrowserDialogFormSettings.SelectedPath = directoryScan;

                textBoxTemplate.Text = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_TEMPLATE_SCANS_FILES);

                textBoxBackupRow.Text = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_BACKUP_SCANS_FILES);

                textBoxIntegrityRow.Text = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_INTEGRITY_SCANS_FILES);
                //необходимо обновить статусы, ибо происходит событие (textBox_TextChanged) при заполнении текстовых полей
                StatusUpdateOnTheForm();
            }
        }

        private void buttonSetSettings_Click(object sender, EventArgs e)
        {
            #region Проверка заполнения полей перед сохранением
            if (textBoxTemplate.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Маска для имени файла отчетов", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (textBoxBackupRow.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Номер строки для проверки копий backup", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (textBoxIntegrityRow.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле - Номер строки для проверки целостности копий", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (folderBrowserDialogFormSettings.SelectedPath.Length == 0)
            {
                DialogResult dialogResult = MessageBox.Show("Укажите с помощью диалога каталог для сканирования. Продолжить?", "Settings.ini", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (dialogResult == DialogResult.OK)
                {
                    buttonSelectedDirectory_Click(null, EventArgs.Empty);

                    if (folderBrowserDialogFormSettings.SelectedPath.Length == 0)
                    {
                        MessageBox.Show("Вы не выбрали каталог для сканирования!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (!labelChoisePathSettings.Text.Contains(folderBrowserDialogFormSettings.SelectedPath))
                    {
                        MessageBox.Show("Программная ошибка, сохранение невозможно!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else if (!labelChoisePathSettings.Text.Contains(folderBrowserDialogFormSettings.SelectedPath))
            {
                MessageBox.Show("Программная ошибка, сохранение невозможно!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } 
            #endregion

            ClearingFieldsBeforeSaving();

            try
            {
                BootLoader.CreateAndUpdateFileSettings(folderBrowserDialogFormSettings.SelectedPath, textBoxTemplate.Text, textBoxBackupRow.Text, textBoxIntegrityRow.Text);

                Helper.Rescan = true;

                StatusUpdateOnTheForm();

                MessageBox.Show("Настройки сохранены успешно!", "Изменение настроек", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxRows_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)48 && e.KeyChar <= (char)57 && ((TextBox)sender).TextLength < 3) || e.KeyChar == (char)Keys.Back)
            {
                if (e.KeyChar == (char)Keys.D0 && ((TextBox)sender).TextLength == 0)
                {
                    e.Handled = true;
                }

                return;
            }

            e.Handled = true;
        }

        private void ClearingFieldsBeforeSaving()
        {
            textBoxBackupRow.Text = textBoxBackupRow.Text.TrimStart('0');

            textBoxIntegrityRow.Text = textBoxIntegrityRow.Text.TrimStart('0');

            textBoxTemplate.Text = textBoxTemplate.Text.Trim();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            toolStripStatusLabelFormSettings.Text = "Необходимо выполнить сохранение настроек";
        }
    }
}
