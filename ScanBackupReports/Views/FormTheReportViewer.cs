﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScanBackupReports.Views
{
    public partial class FormTheReportViewer : Form
    {
        private List<FileReport> reports;

        public FormTheReportViewer()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            if (Helper.HubForReports is List<FileReport>)
            {
                reports = (List<FileReport>)Helper.HubForReports;
            }
            else
            {
                reports = null;
            }
        }

        private void FormTheReportViewer_Load(object sender, EventArgs e)
        {
            radioButtonEntireReport.Checked = true;
        }

        private void listBoxViewAllReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxViewAllReports.SelectedItem != null && listBoxViewAllReports.SelectedItem is FileReport)
            {
                StatusUpdateOnTheForm();

                textBoxViewingSelectedReport.Clear();

                textBoxViewingSelectedReport.Text = ((FileReport)listBoxViewAllReports.SelectedItem).GetAllTextOfContent();
            }
        }

        private void radioButtonTheReportViewer_CheckedChanged(object sender, EventArgs e)
        {
            if (labelBefore.Visible)
            {
                VisibilityOfControls(false);
            }

            if (sender is RadioButton)
            {
                RadioButton radioButton = (RadioButton)sender;

                if (radioButton.Checked)
                {
                    listBoxViewAllReports.Items.Clear();

                    if (radioButton.Text == "Все отчёты")
                    {
                        listBoxViewAllReports.Items.AddRange(reports.ToArray());
                    }
                    else if (radioButton.Text == "Успешные")
                    {
                        listBoxViewAllReports.Items.AddRange(reports.FindAll(item => item.BackupIsCorrect == true).ToArray());

                        StatusUpdateOnTheForm();
                    }
                    else if (radioButton.Text == "Неуспешные")
                    {
                        listBoxViewAllReports.Items.AddRange(reports.FindAll(item => item.BackupIsCorrect == false).ToArray());

                        StatusUpdateOnTheForm();
                    }
                    else if (radioButton.Text == "За временной промежуток")
                    {
                        StatusUpdateOnTheForm();

                        VisibilityOfControls(true);
                    }

                    if (listBoxViewAllReports.Items.Count != 0)
                    {
                        listBoxViewAllReports.SelectedIndex = 0;
                    }
                    else
                    {
                        textBoxViewingSelectedReport.Clear();
                    }
                    
                }
            }
        }

        private void VisibilityOfControls(bool controlSwitch)
        {
            labelFrom.Visible = controlSwitch;
            labelBefore.Visible = controlSwitch;
            dateTimePickerMinDate.Visible = controlSwitch;
            dateTimePickerMaxDate.Visible = controlSwitch;
            buttonFilterDate.Visible = controlSwitch;
        }

        private void buttonFilterDate_Click(object sender, EventArgs e)
        {
            DateTime minDate = dateTimePickerMinDate.Value;
            DateTime maxDate = dateTimePickerMaxDate.Value;

            if (minDate.Date < maxDate.Date)
            {
                listBoxViewAllReports.Items.Clear();

                listBoxViewAllReports.Items.AddRange(reports.FindAll(item => item.DateModified.Date >= minDate.Date
                && item.DateModified.Date <= maxDate.Date).ToArray());
            }
            else if (minDate.Date == maxDate.Date)
            {
                listBoxViewAllReports.Items.Clear();

                listBoxViewAllReports.Items.AddRange(reports.FindAll(item => item.DateModified.Date == minDate.Date).ToArray());
            }
            else
            {
                toolStripStatusLabelFormViewAllReports.Text = "Неверный диапазон дат!";

                return;
            }

            if (listBoxViewAllReports.Items.Count != 0)
            {
                listBoxViewAllReports.SelectedIndex = 0;
            }
            else
            {
                textBoxViewingSelectedReport.Clear();
            }

            StatusUpdateOnTheForm();
        }

        private void StatusUpdateOnTheForm()
        {
            if (listBoxViewAllReports.SelectedItem != null && listBoxViewAllReports.SelectedItem is FileReport)
            {
                FileReport report = (FileReport)listBoxViewAllReports.SelectedItem;

                toolStripStatusLabelFormViewAllReports.Text = "Всего в списке: " + listBoxViewAllReports.Items.Count + ";  " + report.Name + " " + report.SizeKbyte.ToString("0.00") + " Kbyte";
            }
            else
            {
                toolStripStatusLabelFormViewAllReports.Text = String.Empty;
            }
        }

        private void FileCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBoxViewAllReports.SelectedItem != null)
            {
                Helper.HubForFileReport = listBoxViewAllReports.SelectedItem;

                new FileCard().ShowDialog();
            }
        }
    }
}
