﻿namespace ScanBackupReports.Views
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this.statusStripFormSettings = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelFormSettings = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonSelectedDirectory = new System.Windows.Forms.Button();
            this.buttonOpenFileSettings = new System.Windows.Forms.Button();
            this.labelChoisePathSettings = new System.Windows.Forms.Label();
            this.folderBrowserDialogFormSettings = new System.Windows.Forms.FolderBrowserDialog();
            this.textBoxTemplate = new System.Windows.Forms.TextBox();
            this.buttonSetSettings = new System.Windows.Forms.Button();
            this.textBoxBackupRow = new System.Windows.Forms.TextBox();
            this.textBoxIntegrityRow = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            this.statusStripFormSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripFormSettings
            // 
            this.statusStripFormSettings.BackColor = System.Drawing.SystemColors.Control;
            this.statusStripFormSettings.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusStripFormSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelFormSettings});
            this.statusStripFormSettings.Location = new System.Drawing.Point(0, 240);
            this.statusStripFormSettings.Name = "statusStripFormSettings";
            this.statusStripFormSettings.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStripFormSettings.Size = new System.Drawing.Size(484, 22);
            this.statusStripFormSettings.SizingGrip = false;
            this.statusStripFormSettings.TabIndex = 0;
            // 
            // toolStripStatusLabelFormSettings
            // 
            this.toolStripStatusLabelFormSettings.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatusLabelFormSettings.Name = "toolStripStatusLabelFormSettings";
            this.toolStripStatusLabelFormSettings.Size = new System.Drawing.Size(77, 17);
            this.toolStripStatusLabelFormSettings.Text = "Settings.ini: ";
            // 
            // buttonSelectedDirectory
            // 
            this.buttonSelectedDirectory.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonSelectedDirectory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSelectedDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSelectedDirectory.Location = new System.Drawing.Point(368, 120);
            this.buttonSelectedDirectory.Name = "buttonSelectedDirectory";
            this.buttonSelectedDirectory.Size = new System.Drawing.Size(104, 48);
            this.buttonSelectedDirectory.TabIndex = 1;
            this.buttonSelectedDirectory.Text = "Выбрать директорию";
            this.buttonSelectedDirectory.UseVisualStyleBackColor = true;
            this.buttonSelectedDirectory.Click += new System.EventHandler(this.buttonSelectedDirectory_Click);
            // 
            // buttonOpenFileSettings
            // 
            this.buttonOpenFileSettings.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonOpenFileSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonOpenFileSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOpenFileSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOpenFileSettings.ForeColor = System.Drawing.Color.Blue;
            this.buttonOpenFileSettings.Location = new System.Drawing.Point(368, 176);
            this.buttonOpenFileSettings.Name = "buttonOpenFileSettings";
            this.buttonOpenFileSettings.Size = new System.Drawing.Size(104, 48);
            this.buttonOpenFileSettings.TabIndex = 2;
            this.buttonOpenFileSettings.TabStop = false;
            this.buttonOpenFileSettings.Text = "Открыть\r\nSettings.ini";
            this.buttonOpenFileSettings.UseVisualStyleBackColor = true;
            this.buttonOpenFileSettings.Click += new System.EventHandler(this.buttonOpenFileSettings_Click);
            // 
            // labelChoisePathSettings
            // 
            this.labelChoisePathSettings.AutoSize = true;
            this.labelChoisePathSettings.Location = new System.Drawing.Point(16, 16);
            this.labelChoisePathSettings.MaximumSize = new System.Drawing.Size(400, 0);
            this.labelChoisePathSettings.Name = "labelChoisePathSettings";
            this.labelChoisePathSettings.Size = new System.Drawing.Size(126, 13);
            this.labelChoisePathSettings.TabIndex = 3;
            this.labelChoisePathSettings.Text = "Каталог сканирования:";
            // 
            // textBoxTemplate
            // 
            this.textBoxTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTemplate.Location = new System.Drawing.Point(16, 144);
            this.textBoxTemplate.Name = "textBoxTemplate";
            this.textBoxTemplate.Size = new System.Drawing.Size(320, 22);
            this.textBoxTemplate.TabIndex = 2;
            this.textBoxTemplate.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // buttonSetSettings
            // 
            this.buttonSetSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonSetSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSetSettings.Location = new System.Drawing.Point(368, 8);
            this.buttonSetSettings.Name = "buttonSetSettings";
            this.buttonSetSettings.Size = new System.Drawing.Size(104, 48);
            this.buttonSetSettings.TabIndex = 5;
            this.buttonSetSettings.Text = "Сохранить настройки";
            this.buttonSetSettings.UseVisualStyleBackColor = true;
            this.buttonSetSettings.Click += new System.EventHandler(this.buttonSetSettings_Click);
            // 
            // textBoxBackupRow
            // 
            this.textBoxBackupRow.Location = new System.Drawing.Point(288, 176);
            this.textBoxBackupRow.Name = "textBoxBackupRow";
            this.textBoxBackupRow.Size = new System.Drawing.Size(48, 20);
            this.textBoxBackupRow.TabIndex = 3;
            this.textBoxBackupRow.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.textBoxBackupRow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxRows_KeyPress);
            // 
            // textBoxIntegrityRow
            // 
            this.textBoxIntegrityRow.Location = new System.Drawing.Point(288, 208);
            this.textBoxIntegrityRow.Name = "textBoxIntegrityRow";
            this.textBoxIntegrityRow.Size = new System.Drawing.Size(48, 20);
            this.textBoxIntegrityRow.TabIndex = 4;
            this.textBoxIntegrityRow.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            this.textBoxIntegrityRow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxRows_KeyPress);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 120);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(176, 13);
            label1.TabIndex = 10;
            label1.Text = "Маска для имени файла отчетов:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 176);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(226, 13);
            label2.TabIndex = 11;
            label2.Text = "Номер строки для проверки копий backup:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(16, 208);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(254, 13);
            label3.TabIndex = 12;
            label3.Text = "Номер строки для проверки целостности копий:";
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 262);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBoxIntegrityRow);
            this.Controls.Add(this.textBoxBackupRow);
            this.Controls.Add(this.buttonSetSettings);
            this.Controls.Add(this.textBoxTemplate);
            this.Controls.Add(this.labelChoisePathSettings);
            this.Controls.Add(this.buttonOpenFileSettings);
            this.Controls.Add(this.buttonSelectedDirectory);
            this.Controls.Add(this.statusStripFormSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.statusStripFormSettings.ResumeLayout(false);
            this.statusStripFormSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormSettings;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFormSettings;
        private System.Windows.Forms.Button buttonSelectedDirectory;
        private System.Windows.Forms.Button buttonOpenFileSettings;
        private System.Windows.Forms.Label labelChoisePathSettings;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogFormSettings;
        private System.Windows.Forms.TextBox textBoxTemplate;
        private System.Windows.Forms.Button buttonSetSettings;
        private System.Windows.Forms.TextBox textBoxBackupRow;
        private System.Windows.Forms.TextBox textBoxIntegrityRow;
    }
}