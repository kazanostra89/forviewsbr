﻿namespace ScanBackupReports.Views
{
    partial class FormTheReportViewerDataBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TableLayoutPanel tableLayoutPanelTheReportViewer;
            System.Windows.Forms.TableLayoutPanel tableLayoutPanelHeader;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTheReportViewer));
            this.textBoxViewingSelectedReport = new System.Windows.Forms.TextBox();
            this.listBoxViewAllReports = new System.Windows.Forms.ListBox();
            this.groupBoxViewingFilters = new System.Windows.Forms.GroupBox();
            this.buttonFilterDate = new System.Windows.Forms.Button();
            this.labelBefore = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.dateTimePickerMaxDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerMinDate = new System.Windows.Forms.DateTimePicker();
            this.radioButtonSelectionByDate = new System.Windows.Forms.RadioButton();
            this.radioButtonUnsuccessful = new System.Windows.Forms.RadioButton();
            this.radioButtonSuccessful = new System.Windows.Forms.RadioButton();
            this.radioButtonEntireReport = new System.Windows.Forms.RadioButton();
            this.statusStripTheReportViewer = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelFormViewAllReports = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenuViewAllReports = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.FileCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            tableLayoutPanelTheReportViewer = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanelHeader = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanelTheReportViewer.SuspendLayout();
            tableLayoutPanelHeader.SuspendLayout();
            this.groupBoxViewingFilters.SuspendLayout();
            this.statusStripTheReportViewer.SuspendLayout();
            this.contextMenuViewAllReports.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelTheReportViewer
            // 
            tableLayoutPanelTheReportViewer.ColumnCount = 1;
            tableLayoutPanelTheReportViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanelTheReportViewer.Controls.Add(this.textBoxViewingSelectedReport, 0, 1);
            tableLayoutPanelTheReportViewer.Controls.Add(tableLayoutPanelHeader, 0, 0);
            tableLayoutPanelTheReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanelTheReportViewer.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanelTheReportViewer.Name = "tableLayoutPanelTheReportViewer";
            tableLayoutPanelTheReportViewer.RowCount = 2;
            tableLayoutPanelTheReportViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            tableLayoutPanelTheReportViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            tableLayoutPanelTheReportViewer.Size = new System.Drawing.Size(784, 640);
            tableLayoutPanelTheReportViewer.TabIndex = 1;
            // 
            // textBoxViewingSelectedReport
            // 
            this.textBoxViewingSelectedReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxViewingSelectedReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxViewingSelectedReport.Location = new System.Drawing.Point(3, 259);
            this.textBoxViewingSelectedReport.Multiline = true;
            this.textBoxViewingSelectedReport.Name = "textBoxViewingSelectedReport";
            this.textBoxViewingSelectedReport.ReadOnly = true;
            this.textBoxViewingSelectedReport.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxViewingSelectedReport.Size = new System.Drawing.Size(778, 378);
            this.textBoxViewingSelectedReport.TabIndex = 0;
            this.textBoxViewingSelectedReport.TabStop = false;
            // 
            // tableLayoutPanelHeader
            // 
            tableLayoutPanelHeader.ColumnCount = 2;
            tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            tableLayoutPanelHeader.Controls.Add(this.listBoxViewAllReports, 0, 0);
            tableLayoutPanelHeader.Controls.Add(this.groupBoxViewingFilters, 1, 0);
            tableLayoutPanelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanelHeader.Location = new System.Drawing.Point(3, 3);
            tableLayoutPanelHeader.Name = "tableLayoutPanelHeader";
            tableLayoutPanelHeader.RowCount = 1;
            tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanelHeader.Size = new System.Drawing.Size(778, 250);
            tableLayoutPanelHeader.TabIndex = 1;
            // 
            // listBoxViewAllReports
            // 
            this.listBoxViewAllReports.ContextMenuStrip = this.contextMenuViewAllReports;
            this.listBoxViewAllReports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxViewAllReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxViewAllReports.FormattingEnabled = true;
            this.listBoxViewAllReports.ItemHeight = 16;
            this.listBoxViewAllReports.Location = new System.Drawing.Point(3, 3);
            this.listBoxViewAllReports.Name = "listBoxViewAllReports";
            this.listBoxViewAllReports.Size = new System.Drawing.Size(460, 244);
            this.listBoxViewAllReports.TabIndex = 2;
            this.listBoxViewAllReports.TabStop = false;
            this.listBoxViewAllReports.SelectedIndexChanged += new System.EventHandler(this.listBoxViewAllReports_SelectedIndexChanged);
            // 
            // groupBoxViewingFilters
            // 
            this.groupBoxViewingFilters.Controls.Add(this.buttonFilterDate);
            this.groupBoxViewingFilters.Controls.Add(this.labelBefore);
            this.groupBoxViewingFilters.Controls.Add(this.labelFrom);
            this.groupBoxViewingFilters.Controls.Add(this.dateTimePickerMaxDate);
            this.groupBoxViewingFilters.Controls.Add(this.dateTimePickerMinDate);
            this.groupBoxViewingFilters.Controls.Add(this.radioButtonSelectionByDate);
            this.groupBoxViewingFilters.Controls.Add(this.radioButtonUnsuccessful);
            this.groupBoxViewingFilters.Controls.Add(this.radioButtonSuccessful);
            this.groupBoxViewingFilters.Controls.Add(this.radioButtonEntireReport);
            this.groupBoxViewingFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxViewingFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxViewingFilters.Location = new System.Drawing.Point(469, 3);
            this.groupBoxViewingFilters.Name = "groupBoxViewingFilters";
            this.groupBoxViewingFilters.Size = new System.Drawing.Size(306, 244);
            this.groupBoxViewingFilters.TabIndex = 3;
            this.groupBoxViewingFilters.TabStop = false;
            this.groupBoxViewingFilters.Text = "Фильтры для отчетов";
            // 
            // buttonFilterDate
            // 
            this.buttonFilterDate.Location = new System.Drawing.Point(168, 160);
            this.buttonFilterDate.Name = "buttonFilterDate";
            this.buttonFilterDate.Size = new System.Drawing.Size(96, 40);
            this.buttonFilterDate.TabIndex = 8;
            this.buttonFilterDate.Text = "Применить";
            this.buttonFilterDate.UseVisualStyleBackColor = true;
            this.buttonFilterDate.Visible = false;
            this.buttonFilterDate.Click += new System.EventHandler(this.buttonFilterDate_Click);
            // 
            // labelBefore
            // 
            this.labelBefore.AutoSize = true;
            this.labelBefore.Location = new System.Drawing.Point(16, 184);
            this.labelBefore.Name = "labelBefore";
            this.labelBefore.Size = new System.Drawing.Size(28, 16);
            this.labelBefore.TabIndex = 7;
            this.labelBefore.Text = "До:";
            this.labelBefore.Visible = false;
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Location = new System.Drawing.Point(16, 152);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(28, 16);
            this.labelFrom.TabIndex = 6;
            this.labelFrom.Text = "От:";
            this.labelFrom.Visible = false;
            // 
            // dateTimePickerMaxDate
            // 
            this.dateTimePickerMaxDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerMaxDate.Location = new System.Drawing.Point(48, 184);
            this.dateTimePickerMaxDate.Name = "dateTimePickerMaxDate";
            this.dateTimePickerMaxDate.Size = new System.Drawing.Size(104, 22);
            this.dateTimePickerMaxDate.TabIndex = 5;
            this.dateTimePickerMaxDate.TabStop = false;
            this.dateTimePickerMaxDate.Visible = false;
            // 
            // dateTimePickerMinDate
            // 
            this.dateTimePickerMinDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerMinDate.Location = new System.Drawing.Point(48, 152);
            this.dateTimePickerMinDate.Name = "dateTimePickerMinDate";
            this.dateTimePickerMinDate.Size = new System.Drawing.Size(104, 22);
            this.dateTimePickerMinDate.TabIndex = 4;
            this.dateTimePickerMinDate.TabStop = false;
            this.dateTimePickerMinDate.Visible = false;
            // 
            // radioButtonSelectionByDate
            // 
            this.radioButtonSelectionByDate.AutoSize = true;
            this.radioButtonSelectionByDate.Location = new System.Drawing.Point(16, 128);
            this.radioButtonSelectionByDate.Name = "radioButtonSelectionByDate";
            this.radioButtonSelectionByDate.Size = new System.Drawing.Size(202, 20);
            this.radioButtonSelectionByDate.TabIndex = 3;
            this.radioButtonSelectionByDate.Text = "За временной промежуток";
            this.radioButtonSelectionByDate.UseVisualStyleBackColor = true;
            this.radioButtonSelectionByDate.CheckedChanged += new System.EventHandler(this.radioButtonTheReportViewer_CheckedChanged);
            // 
            // radioButtonUnsuccessful
            // 
            this.radioButtonUnsuccessful.AutoSize = true;
            this.radioButtonUnsuccessful.Location = new System.Drawing.Point(16, 96);
            this.radioButtonUnsuccessful.Name = "radioButtonUnsuccessful";
            this.radioButtonUnsuccessful.Size = new System.Drawing.Size(109, 20);
            this.radioButtonUnsuccessful.TabIndex = 2;
            this.radioButtonUnsuccessful.Text = "Неуспешные";
            this.radioButtonUnsuccessful.UseVisualStyleBackColor = true;
            this.radioButtonUnsuccessful.CheckedChanged += new System.EventHandler(this.radioButtonTheReportViewer_CheckedChanged);
            // 
            // radioButtonSuccessful
            // 
            this.radioButtonSuccessful.AutoSize = true;
            this.radioButtonSuccessful.Location = new System.Drawing.Point(16, 64);
            this.radioButtonSuccessful.Name = "radioButtonSuccessful";
            this.radioButtonSuccessful.Size = new System.Drawing.Size(92, 20);
            this.radioButtonSuccessful.TabIndex = 1;
            this.radioButtonSuccessful.Text = "Успешные";
            this.radioButtonSuccessful.UseVisualStyleBackColor = true;
            this.radioButtonSuccessful.CheckedChanged += new System.EventHandler(this.radioButtonTheReportViewer_CheckedChanged);
            // 
            // radioButtonEntireReport
            // 
            this.radioButtonEntireReport.AutoSize = true;
            this.radioButtonEntireReport.Location = new System.Drawing.Point(16, 32);
            this.radioButtonEntireReport.Name = "radioButtonEntireReport";
            this.radioButtonEntireReport.Size = new System.Drawing.Size(100, 20);
            this.radioButtonEntireReport.TabIndex = 0;
            this.radioButtonEntireReport.Text = "Все отчёты";
            this.radioButtonEntireReport.UseVisualStyleBackColor = true;
            this.radioButtonEntireReport.CheckedChanged += new System.EventHandler(this.radioButtonTheReportViewer_CheckedChanged);
            // 
            // statusStripTheReportViewer
            // 
            this.statusStripTheReportViewer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelFormViewAllReports});
            this.statusStripTheReportViewer.Location = new System.Drawing.Point(0, 640);
            this.statusStripTheReportViewer.Name = "statusStripTheReportViewer";
            this.statusStripTheReportViewer.Size = new System.Drawing.Size(784, 22);
            this.statusStripTheReportViewer.SizingGrip = false;
            this.statusStripTheReportViewer.TabIndex = 0;
            this.statusStripTheReportViewer.Text = "statusStrip1";
            // 
            // toolStripStatusLabelFormViewAllReports
            // 
            this.toolStripStatusLabelFormViewAllReports.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripStatusLabelFormViewAllReports.Name = "toolStripStatusLabelFormViewAllReports";
            this.toolStripStatusLabelFormViewAllReports.Size = new System.Drawing.Size(131, 17);
            this.toolStripStatusLabelFormViewAllReports.Text = "toolStripStatusLabel1";
            // 
            // contextMenuViewAllReports
            // 
            this.contextMenuViewAllReports.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileCardToolStripMenuItem});
            this.contextMenuViewAllReports.Name = "contextMenu";
            this.contextMenuViewAllReports.Size = new System.Drawing.Size(164, 48);
            // 
            // FileCardToolStripMenuItem
            // 
            this.FileCardToolStripMenuItem.Name = "FileCardToolStripMenuItem";
            this.FileCardToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.FileCardToolStripMenuItem.Text = "Карточка файла";
            this.FileCardToolStripMenuItem.Click += new System.EventHandler(this.FileCardToolStripMenuItem_Click);
            // 
            // FormTheReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 662);
            this.Controls.Add(tableLayoutPanelTheReportViewer);
            this.Controls.Add(this.statusStripTheReportViewer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormTheReportViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Просмотр отчётов резервного копирования";
            this.Load += new System.EventHandler(this.FormTheReportViewer_Load);
            tableLayoutPanelTheReportViewer.ResumeLayout(false);
            tableLayoutPanelTheReportViewer.PerformLayout();
            tableLayoutPanelHeader.ResumeLayout(false);
            this.groupBoxViewingFilters.ResumeLayout(false);
            this.groupBoxViewingFilters.PerformLayout();
            this.statusStripTheReportViewer.ResumeLayout(false);
            this.statusStripTheReportViewer.PerformLayout();
            this.contextMenuViewAllReports.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripTheReportViewer;
        private System.Windows.Forms.TextBox textBoxViewingSelectedReport;
        private System.Windows.Forms.ListBox listBoxViewAllReports;
        private System.Windows.Forms.GroupBox groupBoxViewingFilters;
        private System.Windows.Forms.RadioButton radioButtonUnsuccessful;
        private System.Windows.Forms.RadioButton radioButtonSuccessful;
        private System.Windows.Forms.RadioButton radioButtonEntireReport;
        private System.Windows.Forms.DateTimePicker dateTimePickerMaxDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerMinDate;
        private System.Windows.Forms.RadioButton radioButtonSelectionByDate;
        private System.Windows.Forms.Button buttonFilterDate;
        private System.Windows.Forms.Label labelBefore;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFormViewAllReports;
        private System.Windows.Forms.ContextMenuStrip contextMenuViewAllReports;
        private System.Windows.Forms.ToolStripMenuItem FileCardToolStripMenuItem;
    }
}