﻿namespace ScanBackupReports.Views
{
    partial class FormDataBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDataBase));
            this.listBoxFromDataBase = new System.Windows.Forms.ListBox();
            this.listBoxUnique = new System.Windows.Forms.ListBox();
            this.listBoxFromScanned = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonTruncateTableReports = new System.Windows.Forms.Button();
            this.buttonUploadToTheDatabase = new System.Windows.Forms.Button();
            this.buttonSearchUnique = new System.Windows.Forms.Button();
            this.buttonLoadScanned = new System.Windows.Forms.Button();
            this.buttonLoadDataBase = new System.Windows.Forms.Button();
            this.statusStripFormDataBase = new System.Windows.Forms.StatusStrip();
            this.statusLabelFormDataBase = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonSaveReportsToFolder = new System.Windows.Forms.Button();
            this.folderToSaveReports = new System.Windows.Forms.FolderBrowserDialog();
            tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStripFormDataBase.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(this.listBoxFromDataBase, 0, 1);
            tableLayoutPanel1.Controls.Add(this.listBoxUnique, 1, 0);
            tableLayoutPanel1.Controls.Add(this.listBoxFromScanned, 0, 0);
            tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new System.Drawing.Size(784, 640);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // listBoxFromDataBase
            // 
            this.listBoxFromDataBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxFromDataBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxFromDataBase.FormattingEnabled = true;
            this.listBoxFromDataBase.ItemHeight = 16;
            this.listBoxFromDataBase.Location = new System.Drawing.Point(3, 323);
            this.listBoxFromDataBase.Name = "listBoxFromDataBase";
            this.listBoxFromDataBase.Size = new System.Drawing.Size(386, 314);
            this.listBoxFromDataBase.TabIndex = 2;
            this.listBoxFromDataBase.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            // 
            // listBoxUnique
            // 
            this.listBoxUnique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxUnique.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxUnique.FormattingEnabled = true;
            this.listBoxUnique.ItemHeight = 16;
            this.listBoxUnique.Location = new System.Drawing.Point(395, 3);
            this.listBoxUnique.Name = "listBoxUnique";
            this.listBoxUnique.Size = new System.Drawing.Size(386, 314);
            this.listBoxUnique.TabIndex = 1;
            this.listBoxUnique.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            // 
            // listBoxFromScanned
            // 
            this.listBoxFromScanned.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxFromScanned.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxFromScanned.FormattingEnabled = true;
            this.listBoxFromScanned.ItemHeight = 16;
            this.listBoxFromScanned.Location = new System.Drawing.Point(3, 3);
            this.listBoxFromScanned.Name = "listBoxFromScanned";
            this.listBoxFromScanned.Size = new System.Drawing.Size(386, 314);
            this.listBoxFromScanned.TabIndex = 0;
            this.listBoxFromScanned.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSaveReportsToFolder);
            this.panel1.Controls.Add(this.buttonTruncateTableReports);
            this.panel1.Controls.Add(this.buttonUploadToTheDatabase);
            this.panel1.Controls.Add(this.buttonSearchUnique);
            this.panel1.Controls.Add(this.buttonLoadScanned);
            this.panel1.Controls.Add(this.buttonLoadDataBase);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(395, 323);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 314);
            this.panel1.TabIndex = 3;
            // 
            // buttonTruncateTableReports
            // 
            this.buttonTruncateTableReports.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonTruncateTableReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTruncateTableReports.Location = new System.Drawing.Point(56, 272);
            this.buttonTruncateTableReports.Name = "buttonTruncateTableReports";
            this.buttonTruncateTableReports.Size = new System.Drawing.Size(296, 32);
            this.buttonTruncateTableReports.TabIndex = 8;
            this.buttonTruncateTableReports.Text = "Полная очистка локальной БД";
            this.buttonTruncateTableReports.UseVisualStyleBackColor = true;
            this.buttonTruncateTableReports.Click += new System.EventHandler(this.buttonTruncateTableReports_Click);
            // 
            // buttonUploadToTheDatabase
            // 
            this.buttonUploadToTheDatabase.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonUploadToTheDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUploadToTheDatabase.Location = new System.Drawing.Point(56, 152);
            this.buttonUploadToTheDatabase.Name = "buttonUploadToTheDatabase";
            this.buttonUploadToTheDatabase.Size = new System.Drawing.Size(296, 32);
            this.buttonUploadToTheDatabase.TabIndex = 7;
            this.buttonUploadToTheDatabase.Text = "Выгрузить в БД";
            this.buttonUploadToTheDatabase.UseVisualStyleBackColor = true;
            this.buttonUploadToTheDatabase.Click += new System.EventHandler(this.buttonUploadToTheDatabase_Click);
            // 
            // buttonSearchUnique
            // 
            this.buttonSearchUnique.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonSearchUnique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchUnique.Location = new System.Drawing.Point(56, 112);
            this.buttonSearchUnique.Name = "buttonSearchUnique";
            this.buttonSearchUnique.Size = new System.Drawing.Size(296, 32);
            this.buttonSearchUnique.TabIndex = 6;
            this.buttonSearchUnique.Text = "Найти отличающиеся";
            this.buttonSearchUnique.UseVisualStyleBackColor = true;
            this.buttonSearchUnique.Click += new System.EventHandler(this.buttonSearchUnique_Click);
            // 
            // buttonLoadScanned
            // 
            this.buttonLoadScanned.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonLoadScanned.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadScanned.Location = new System.Drawing.Point(56, 56);
            this.buttonLoadScanned.Name = "buttonLoadScanned";
            this.buttonLoadScanned.Size = new System.Drawing.Size(296, 32);
            this.buttonLoadScanned.TabIndex = 5;
            this.buttonLoadScanned.Text = "Загрузить результаты сканирования\r\n";
            this.buttonLoadScanned.UseVisualStyleBackColor = true;
            this.buttonLoadScanned.Click += new System.EventHandler(this.buttonLoadScanned_Click);
            // 
            // buttonLoadDataBase
            // 
            this.buttonLoadDataBase.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonLoadDataBase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadDataBase.Location = new System.Drawing.Point(56, 16);
            this.buttonLoadDataBase.Name = "buttonLoadDataBase";
            this.buttonLoadDataBase.Size = new System.Drawing.Size(296, 32);
            this.buttonLoadDataBase.TabIndex = 4;
            this.buttonLoadDataBase.Text = "Загрузить из БД";
            this.buttonLoadDataBase.UseVisualStyleBackColor = true;
            this.buttonLoadDataBase.Click += new System.EventHandler(this.buttonLoadDataBase_Click);
            // 
            // statusStripFormDataBase
            // 
            this.statusStripFormDataBase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelFormDataBase});
            this.statusStripFormDataBase.Location = new System.Drawing.Point(0, 640);
            this.statusStripFormDataBase.Name = "statusStripFormDataBase";
            this.statusStripFormDataBase.Size = new System.Drawing.Size(784, 22);
            this.statusStripFormDataBase.SizingGrip = false;
            this.statusStripFormDataBase.TabIndex = 1;
            this.statusStripFormDataBase.Text = "statusStrip1";
            // 
            // statusLabelFormDataBase
            // 
            this.statusLabelFormDataBase.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusLabelFormDataBase.ForeColor = System.Drawing.Color.Blue;
            this.statusLabelFormDataBase.Name = "statusLabelFormDataBase";
            this.statusLabelFormDataBase.Size = new System.Drawing.Size(131, 17);
            this.statusLabelFormDataBase.Text = "toolStripStatusLabel1";
            // 
            // buttonSaveReportsToFolder
            // 
            this.buttonSaveReportsToFolder.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonSaveReportsToFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveReportsToFolder.Location = new System.Drawing.Point(56, 192);
            this.buttonSaveReportsToFolder.Name = "buttonSaveReportsToFolder";
            this.buttonSaveReportsToFolder.Size = new System.Drawing.Size(296, 32);
            this.buttonSaveReportsToFolder.TabIndex = 9;
            this.buttonSaveReportsToFolder.Text = "Сохранить отчеты в каталог на диске";
            this.buttonSaveReportsToFolder.UseVisualStyleBackColor = true;
            this.buttonSaveReportsToFolder.Click += new System.EventHandler(this.buttonSaveReportsToFolder_Click);
            // 
            // FormDataBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 662);
            this.Controls.Add(tableLayoutPanel1);
            this.Controls.Add(this.statusStripFormDataBase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDataBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Локальная БД";
            this.Load += new System.EventHandler(this.FormDataBase_Load);
            tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.statusStripFormDataBase.ResumeLayout(false);
            this.statusStripFormDataBase.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripFormDataBase;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFormDataBase;
        private System.Windows.Forms.ListBox listBoxFromDataBase;
        private System.Windows.Forms.ListBox listBoxUnique;
        private System.Windows.Forms.ListBox listBoxFromScanned;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonUploadToTheDatabase;
        private System.Windows.Forms.Button buttonSearchUnique;
        private System.Windows.Forms.Button buttonLoadScanned;
        private System.Windows.Forms.Button buttonLoadDataBase;
        private System.Windows.Forms.Button buttonTruncateTableReports;
        private System.Windows.Forms.Button buttonSaveReportsToFolder;
        private System.Windows.Forms.FolderBrowserDialog folderToSaveReports;
    }
}