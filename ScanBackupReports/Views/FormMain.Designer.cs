﻿namespace ScanBackupReports.Views
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.GroupBox groupBoxFormMainScanResult;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelScanResult = new System.Windows.Forms.Label();
            this.buttonViewAllReports = new System.Windows.Forms.Button();
            this.buttonScan = new System.Windows.Forms.Button();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.statusStripFormMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelFormMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonDB = new System.Windows.Forms.Button();
            this.notifyIconFormMain = new System.Windows.Forms.NotifyIcon(this.components);
            groupBoxFormMainScanResult = new System.Windows.Forms.GroupBox();
            groupBoxFormMainScanResult.SuspendLayout();
            this.statusStripFormMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFormMainScanResult
            // 
            groupBoxFormMainScanResult.Controls.Add(this.labelScanResult);
            groupBoxFormMainScanResult.Location = new System.Drawing.Point(8, 16);
            groupBoxFormMainScanResult.Name = "groupBoxFormMainScanResult";
            groupBoxFormMainScanResult.Size = new System.Drawing.Size(240, 184);
            groupBoxFormMainScanResult.TabIndex = 5;
            groupBoxFormMainScanResult.TabStop = false;
            groupBoxFormMainScanResult.Text = "Результаты сканирования";
            // 
            // labelScanResult
            // 
            this.labelScanResult.AutoEllipsis = true;
            this.labelScanResult.AutoSize = true;
            this.labelScanResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelScanResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelScanResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelScanResult.Location = new System.Drawing.Point(3, 16);
            this.labelScanResult.MaximumSize = new System.Drawing.Size(230, 0);
            this.labelScanResult.Name = "labelScanResult";
            this.labelScanResult.Size = new System.Drawing.Size(30, 15);
            this.labelScanResult.TabIndex = 0;
            this.labelScanResult.Text = "Text";
            // 
            // buttonViewAllReports
            // 
            this.buttonViewAllReports.Enabled = false;
            this.buttonViewAllReports.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonViewAllReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonViewAllReports.Location = new System.Drawing.Point(304, 88);
            this.buttonViewAllReports.Name = "buttonViewAllReports";
            this.buttonViewAllReports.Size = new System.Drawing.Size(88, 48);
            this.buttonViewAllReports.TabIndex = 0;
            this.buttonViewAllReports.Text = "Просмотр\r\nотчетов";
            this.buttonViewAllReports.UseVisualStyleBackColor = true;
            this.buttonViewAllReports.Click += new System.EventHandler(this.buttonViewAllReports_Click);
            // 
            // buttonScan
            // 
            this.buttonScan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonScan.Location = new System.Drawing.Point(304, 24);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(88, 48);
            this.buttonScan.TabIndex = 2;
            this.buttonScan.Text = "Сканировать";
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // buttonSettings
            // 
            this.buttonSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSettings.Image = global::ScanBackupReports.Properties.Resources.icons8_шестерни_40;
            this.buttonSettings.Location = new System.Drawing.Point(336, 152);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(56, 48);
            this.buttonSettings.TabIndex = 3;
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // statusStripFormMain
            // 
            this.statusStripFormMain.BackColor = System.Drawing.SystemColors.Control;
            this.statusStripFormMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelFormMain});
            this.statusStripFormMain.Location = new System.Drawing.Point(0, 220);
            this.statusStripFormMain.Name = "statusStripFormMain";
            this.statusStripFormMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStripFormMain.Size = new System.Drawing.Size(404, 22);
            this.statusStripFormMain.SizingGrip = false;
            this.statusStripFormMain.TabIndex = 4;
            this.statusStripFormMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabelFormMain
            // 
            this.toolStripStatusLabelFormMain.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabelFormMain.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripStatusLabelFormMain.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatusLabelFormMain.Name = "toolStripStatusLabelFormMain";
            this.toolStripStatusLabelFormMain.Size = new System.Drawing.Size(77, 17);
            this.toolStripStatusLabelFormMain.Text = "Settings.ini: ";
            // 
            // buttonDB
            // 
            this.buttonDB.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Aquamarine;
            this.buttonDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDB.Image = global::ScanBackupReports.Properties.Resources.database_configuration;
            this.buttonDB.Location = new System.Drawing.Point(264, 152);
            this.buttonDB.Name = "buttonDB";
            this.buttonDB.Size = new System.Drawing.Size(56, 48);
            this.buttonDB.TabIndex = 6;
            this.buttonDB.UseVisualStyleBackColor = true;
            this.buttonDB.Click += new System.EventHandler(this.buttonDB_Click);
            // 
            // notifyIconFormMain
            // 
            this.notifyIconFormMain.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconFormMain.Icon")));
            this.notifyIconFormMain.Text = "Программа для сканирования файлов отчетов";
            this.notifyIconFormMain.Visible = true;
            this.notifyIconFormMain.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIconFormMain_MouseDoubleClick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(404, 242);
            this.Controls.Add(this.buttonDB);
            this.Controls.Add(groupBoxFormMainScanResult);
            this.Controls.Add(this.statusStripFormMain);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.buttonScan);
            this.Controls.Add(this.buttonViewAllReports);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проверка файлов отчётов";
            this.Activated += new System.EventHandler(this.FormMain_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            groupBoxFormMainScanResult.ResumeLayout(false);
            groupBoxFormMainScanResult.PerformLayout();
            this.statusStripFormMain.ResumeLayout(false);
            this.statusStripFormMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonViewAllReports;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.StatusStrip statusStripFormMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFormMain;
        private System.Windows.Forms.Label labelScanResult;
        private System.Windows.Forms.Button buttonDB;
        private System.Windows.Forms.NotifyIcon notifyIconFormMain;
    }
}

