﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScanBackupReports.Views
{
    public partial class FormMain : Form
    {
        private TaskManager manager;

        public FormMain()
        {
            InitializeComponent();
            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            manager = new TaskManager();

            Helper.NotifyIconMessages += new Helper.NotifyIconHandler(MessagesNotifyIcon);
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            using (FormSettings formSettings = new FormSettings())
            {
                formSettings.ShowDialog();
            }
        }

        private void buttonViewAllReports_Click(object sender, EventArgs e)
        {
            using (FormTheReportViewer formTheReportViewer = new FormTheReportViewer())
            {
                formTheReportViewer.ShowDialog();
            }
        }

        private async void buttonScan_Click(object sender, EventArgs e)
        {
            buttonScan.Enabled = false;

            try
            {
                await Task.Run(new Action(manager.LoadReportFiles));

                if (manager.CountReports != 0)
                {
                    labelScanResult.Text = manager.ScanResult();
                    Helper.CallMessageForNotifyIcon("Сканирование", manager.ScanResult(), ToolTipIcon.Info);
                    buttonViewAllReports.Enabled = true;
                }
                else
                {
                    labelScanResult.Text = "В указанной директории - отчётов не найдено!";

                    buttonViewAllReports.Enabled = false;
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                labelScanResult.Text = error.Message;

                buttonViewAllReports.Enabled = false;
            }

            if (Helper.Rescan)
            {
                Helper.Rescan = false;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            labelScanResult.Text = String.Empty;

            if (BootLoader.StatusFileSettings == StatusFileSettings.CorrectDirectoryIsReadyForScanning)
            {
                buttonScan.Enabled = true;
            }
            else
            {
                buttonScan.Enabled = false;
            }

            if (BootLoader.StatusFileSettings == StatusFileSettings.NoFile)
            {
                DialogResult result = MessageBox.Show("Отсутствует файл настройки приложения - Settings.ini\nСоздать файл настройки по указанному пути:\n" + BootLoader.PathFileSettings, "Settings.ini", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    try
                    {
                        BootLoader.CreateAndUpdateFileSettings(String.Empty, String.Empty, String.Empty, String.Empty);

                        MessageBox.Show("Файл настроек Settings.ini успешно создан!\n\n" + BootLoader.PathFileSettings, "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception errorCreateFile)
                    {
                        MessageBox.Show(errorCreateFile.Message + "\n\nЭлементы управления заблокированны! Для продолжения работы с приложением, необходимо перейти в меню настроек!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Элементы управления заблокированны!\nДля продолжения работы с приложением,\nнеобходимо перейти в меню настроек!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (BootLoader.StatusFileSettings == StatusFileSettings.IncorrectCheckTheContents)
            {
                DialogResult dialogResult = MessageBox.Show("Некорректное содержимое файла настроек Settings.ini\n\n Перейти к настройкам?", "Settings.ini", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                
                if (dialogResult == DialogResult.OK)
                {
                    using (FormSettings formSettings = new FormSettings())
                    {
                        formSettings.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Элементы управления заблокированны!\nДля продолжения работы с приложением,\nнеобходимо перейти в меню настроек!", "Settings.ini", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            StatusUpdateOnTheForm();
        }

        private void FormMain_Activated(object sender, EventArgs e)
        {
            StatusUpdateOnTheForm();
        }

        
        private void StatusUpdateOnTheForm()
        {
            toolStripStatusLabelFormMain.Text = "Settings.ini:  " + BootLoader.GetCorrectValueStatusFileSetting();

            if (Helper.Rescan)
            {
                buttonScan.Enabled = true;

                buttonViewAllReports.Enabled = false;
            }
        }

        private void buttonDB_Click(object sender, EventArgs e)
        {
            using (FormDataBase formDataBase = new FormDataBase())
            {
                formDataBase.ShowDialog();
            }
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized && notifyIconFormMain.Visible == true)
            {
                this.Hide();

                Helper.CallMessageForNotifyIcon("Информация", "Программа свернута, но продолжает работать!", ToolTipIcon.Info);
            }
        }

        private void notifyIconFormMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized && notifyIconFormMain.Visible == true)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void MessagesNotifyIcon(string title, string message, ToolTipIcon icon)
        {
            if (notifyIconFormMain.Visible == true)
            {
                notifyIconFormMain.ShowBalloonTip(2500, title, message, icon);
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifyIconFormMain.Visible = false;
        }
    }
}
