﻿namespace ScanBackupReports.Views
{
    partial class FileCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            this.textBoxFileCardName = new System.Windows.Forms.TextBox();
            this.textBoxFileCardDirectoryName = new System.Windows.Forms.TextBox();
            this.textBoxFileCardDateCreation = new System.Windows.Forms.TextBox();
            this.textBoxFileCardDateModified = new System.Windows.Forms.TextBox();
            this.textBoxFileCardStatus = new System.Windows.Forms.TextBox();
            this.textBoxFileCardSize = new System.Windows.Forms.TextBox();
            this.textBoxHashMD5 = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.ForeColor = System.Drawing.Color.Blue;
            label1.Location = new System.Drawing.Point(8, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(83, 16);
            label1.TabIndex = 0;
            label1.Text = "Имя файла:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.ForeColor = System.Drawing.Color.Blue;
            label2.Location = new System.Drawing.Point(8, 80);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(162, 16);
            label2.TabIndex = 1;
            label2.Text = "Каталог расположения:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.ForeColor = System.Drawing.Color.Blue;
            label3.Location = new System.Drawing.Point(8, 192);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(108, 16);
            label3.TabIndex = 3;
            label3.Text = "Дата создания:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.ForeColor = System.Drawing.Color.Blue;
            label4.Location = new System.Drawing.Point(192, 192);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(137, 16);
            label4.TabIndex = 2;
            label4.Text = "Дата модификации:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.ForeColor = System.Drawing.Color.Blue;
            label5.Location = new System.Drawing.Point(376, 192);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(123, 16);
            label5.TabIndex = 8;
            label5.Text = "Статус проверки:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.ForeColor = System.Drawing.Color.Blue;
            label6.Location = new System.Drawing.Point(8, 248);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(83, 16);
            label6.TabIndex = 10;
            label6.Text = "Размер, Кб:";
            // 
            // textBoxFileCardName
            // 
            this.textBoxFileCardName.Location = new System.Drawing.Point(8, 40);
            this.textBoxFileCardName.Name = "textBoxFileCardName";
            this.textBoxFileCardName.ReadOnly = true;
            this.textBoxFileCardName.Size = new System.Drawing.Size(510, 22);
            this.textBoxFileCardName.TabIndex = 4;
            this.textBoxFileCardName.TabStop = false;
            // 
            // textBoxFileCardDirectoryName
            // 
            this.textBoxFileCardDirectoryName.Location = new System.Drawing.Point(8, 104);
            this.textBoxFileCardDirectoryName.Multiline = true;
            this.textBoxFileCardDirectoryName.Name = "textBoxFileCardDirectoryName";
            this.textBoxFileCardDirectoryName.ReadOnly = true;
            this.textBoxFileCardDirectoryName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxFileCardDirectoryName.Size = new System.Drawing.Size(510, 50);
            this.textBoxFileCardDirectoryName.TabIndex = 5;
            this.textBoxFileCardDirectoryName.TabStop = false;
            // 
            // textBoxFileCardDateCreation
            // 
            this.textBoxFileCardDateCreation.Location = new System.Drawing.Point(8, 216);
            this.textBoxFileCardDateCreation.Name = "textBoxFileCardDateCreation";
            this.textBoxFileCardDateCreation.ReadOnly = true;
            this.textBoxFileCardDateCreation.Size = new System.Drawing.Size(140, 22);
            this.textBoxFileCardDateCreation.TabIndex = 6;
            this.textBoxFileCardDateCreation.TabStop = false;
            // 
            // textBoxFileCardDateModified
            // 
            this.textBoxFileCardDateModified.Location = new System.Drawing.Point(192, 216);
            this.textBoxFileCardDateModified.Name = "textBoxFileCardDateModified";
            this.textBoxFileCardDateModified.ReadOnly = true;
            this.textBoxFileCardDateModified.Size = new System.Drawing.Size(140, 22);
            this.textBoxFileCardDateModified.TabIndex = 7;
            this.textBoxFileCardDateModified.TabStop = false;
            // 
            // textBoxFileCardStatus
            // 
            this.textBoxFileCardStatus.Location = new System.Drawing.Point(376, 216);
            this.textBoxFileCardStatus.Name = "textBoxFileCardStatus";
            this.textBoxFileCardStatus.ReadOnly = true;
            this.textBoxFileCardStatus.Size = new System.Drawing.Size(140, 22);
            this.textBoxFileCardStatus.TabIndex = 9;
            this.textBoxFileCardStatus.TabStop = false;
            // 
            // textBoxFileCardSize
            // 
            this.textBoxFileCardSize.Location = new System.Drawing.Point(8, 272);
            this.textBoxFileCardSize.Name = "textBoxFileCardSize";
            this.textBoxFileCardSize.ReadOnly = true;
            this.textBoxFileCardSize.Size = new System.Drawing.Size(88, 22);
            this.textBoxFileCardSize.TabIndex = 11;
            this.textBoxFileCardSize.TabStop = false;
            // 
            // textBoxHashMD5
            // 
            this.textBoxHashMD5.Location = new System.Drawing.Point(192, 272);
            this.textBoxHashMD5.Name = "textBoxHashMD5";
            this.textBoxHashMD5.ReadOnly = true;
            this.textBoxHashMD5.Size = new System.Drawing.Size(320, 22);
            this.textBoxHashMD5.TabIndex = 12;
            this.textBoxHashMD5.TabStop = false;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.ForeColor = System.Drawing.Color.Blue;
            label7.Location = new System.Drawing.Point(192, 248);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(39, 16);
            label7.TabIndex = 13;
            label7.Text = "MD5:";
            // 
            // FileCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 312);
            this.Controls.Add(label7);
            this.Controls.Add(this.textBoxHashMD5);
            this.Controls.Add(this.textBoxFileCardSize);
            this.Controls.Add(label6);
            this.Controls.Add(this.textBoxFileCardStatus);
            this.Controls.Add(label5);
            this.Controls.Add(this.textBoxFileCardDateModified);
            this.Controls.Add(this.textBoxFileCardDateCreation);
            this.Controls.Add(this.textBoxFileCardDirectoryName);
            this.Controls.Add(this.textBoxFileCardName);
            this.Controls.Add(label3);
            this.Controls.Add(label4);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FileCard";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Карточка файла отчета";
            this.Load += new System.EventHandler(this.FileCard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFileCardName;
        private System.Windows.Forms.TextBox textBoxFileCardDirectoryName;
        private System.Windows.Forms.TextBox textBoxFileCardDateCreation;
        private System.Windows.Forms.TextBox textBoxFileCardDateModified;
        private System.Windows.Forms.TextBox textBoxFileCardStatus;
        private System.Windows.Forms.TextBox textBoxFileCardSize;
        private System.Windows.Forms.TextBox textBoxHashMD5;
    }
}