﻿using System;

namespace ScanBackupReports
{
    /// <summary>
    /// Используется для определения готовности файла настроек к работе с программой
    /// </summary>
    enum StatusFileSettings
    {
        /// <summary>
        /// Отсутствует файл настроек "Settings.ini"
        /// </summary>
        NoFile = 0,

        /// <summary>
        /// Файл настроек есть, но проверку по наличию всех ключей не прошел. Необходимо проверить содержимое файла
        /// Словарь может содержать лишние данные
        /// </summary>
        IncorrectCheckTheContents,

        /// <summary>
        /// Файл настроек и все ключи есть, директория для сканирования корректна
        /// </summary>
        CorrectDirectoryIsReadyForScanning,

        /// <summary>
        /// Создан новый файл настроек с необходимыми ключами, но пустыми значениями
        /// </summary>
        CorrectNoDirectoryForScanning
    }
}
