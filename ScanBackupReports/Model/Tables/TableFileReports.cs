﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanBackupReports
{
    class TableFileReports
    {
        private SQLiteConnection connection;

        public TableFileReports(SQLiteConnection connection)
        {
            this.connection = connection;
        }

        public List<FileReport> GetAllFileReports()
        {
            List<FileReport> fileReports = new List<FileReport>();

            using (SQLiteCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Reports;";

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        fileReports.Add(new FileReport(
                            reader.GetString(reader.GetOrdinal("Name")),
                            reader.GetString(reader.GetOrdinal("DirectoryName")),
                            reader.GetString(reader.GetOrdinal("Md5")),
                            reader.GetInt64(reader.GetOrdinal("FileSize")),
                            reader.GetDateTime(reader.GetOrdinal("CreationDate")),
                            reader.GetDateTime(reader.GetOrdinal("ModifiedDate")),
                            (byte[])reader.GetValue(reader.GetOrdinal("Content")),
                            reader.GetBoolean(reader.GetOrdinal("BackupIsCorrect"))
                            ));
                    }
                }
            }

            return fileReports;
        }

        public bool InsertFileReports(List<FileReport> fileReports)
        {
            int resultOperation = 0;

            using (SQLiteTransaction transaction = connection.BeginTransaction())
            {
                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.Transaction = transaction;

                    command.CommandText = "INSERT INTO Reports (Name, DirectoryName, CreationDate, ModifiedDate, Md5, Content, BackupIsCorrect, FileSize) VALUES (@Name, @DirectoryName, @CreationDate, @ModifiedDate, @Md5, @Content, @BackupIsCorrect, @FileSize);";

                    command.Parameters.Add("@Name", DbType.String);
                    command.Parameters.Add("@DirectoryName", DbType.String);
                    command.Parameters.Add("@CreationDate", DbType.DateTime);
                    command.Parameters.Add("@ModifiedDate", DbType.DateTime);
                    command.Parameters.Add("@Md5", DbType.String);
                    command.Parameters.Add("@Content", DbType.Binary);
                    command.Parameters.Add("@BackupIsCorrect", DbType.Boolean);
                    command.Parameters.Add("@FileSize", DbType.Int64);

                    try
                    {
                        foreach (FileReport file in fileReports)
                        {
                            command.Parameters["@Name"].Value = file.Name;
                            command.Parameters["@DirectoryName"].Value = file.FullName;
                            command.Parameters["@CreationDate"].Value = file.DateCreation;
                            command.Parameters["@ModifiedDate"].Value = file.DateModified;
                            command.Parameters["@Md5"].Value = file.MD5;
                            command.Parameters["@Content"].Value = file.Content;
                            command.Parameters["@BackupIsCorrect"].Value = file.BackupIsCorrect;
                            command.Parameters["@FileSize"].Value = file.Size;

                            resultOperation += command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }

            return resultOperation == fileReports.Count;
        }

        public void TruncateTableReports()
        {
            using (SQLiteCommand command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM Reports; DELETE FROM sqlite_sequence WHERE name='Reports'; VACUUM;";

                command.ExecuteNonQuery();
            }
        }
    }
}
