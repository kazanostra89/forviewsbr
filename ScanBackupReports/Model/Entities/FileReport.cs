﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScanBackupReports
{
    class FileReport
    {
        private string name;
        private string fullName;
        private string hashMD5;
        private long size;
        private DateTime dateCreation;
        private DateTime dateModified;
        private byte[] content;
        private bool backupIsCorrect;

        public FileReport(string name, string fullName, string hashMD5, long size, DateTime dateCreation, DateTime dateModified, byte[] content, bool backupIsCorrect)
        {
            this.name = name;
            this.fullName = fullName;
            this.hashMD5 = hashMD5;
            this.size = size;
            this.dateCreation = dateCreation;
            this.dateModified = dateModified;
            this.content = content;
            this.backupIsCorrect = backupIsCorrect;
        }

        public string GetAllTextOfContent()
        {
            return Encoding.UTF8.GetString(content);
        }

        public override string ToString()
        {
            //return "Резервная копия Search от " + dateCreation.ToShortDateString() + " в " + dateCreation.ToLongTimeString();
            return "Резервная копия Search от " + dateModified.Date.ToShortDateString() + " в " + dateModified.ToLongTimeString();
            //return base.ToString();
        }


        public string Name
        {
            get { return name; }
        }

        public string FullName
        {
            get { return fullName; }
        }

        public string DirectoryName
        {
            get { return fullName.Remove(fullName.LastIndexOf("\\")); }
        }

        public string MD5
        {
            get { return hashMD5; }
        }

        public double SizeKbyte
        {
            get { return (double)size * 0.0009765625; }
        }

        public DateTime DateCreation
        {
            get { return dateCreation; }
        }

        public DateTime DateModified
        {
            get { return dateModified; }
        }

        public bool BackupIsCorrect
        {
            get { return backupIsCorrect; }
        }

        public byte[] Content
        {
            get { return content; }
        }

        public long Size
        {
            get { return size; }
        }

    }
}
