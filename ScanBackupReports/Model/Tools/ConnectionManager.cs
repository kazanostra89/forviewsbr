﻿using System;
using System.Data.SQLite;
using System.Configuration;

namespace ScanBackupReports
{
    static class ConnectionManager
    {
        public static SQLiteConnection GetConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["strSQLite"].ConnectionString;

            return new SQLiteConnection(connectionString);
        }
    }
}
