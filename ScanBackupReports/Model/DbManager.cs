﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanBackupReports
{
    sealed class DbManager
    {
        private static DbManager manager = null;
        private SQLiteConnection connection;

        private DbManager()
        {
            connection = ConnectionManager.GetConnection();
            TableFileReports = new TableFileReports(connection);
        }

        public static DbManager GetDbManager()
        {
            if (manager == null)
            {
                manager = new DbManager();
            }

            return manager;
        }

        public void ConnectionOpen()
        {
            connection.Open();
        }

        public void ConnectionClose()
        {
            if (connection.State != ConnectionState.Closed) connection.Close();
        }

        public bool CreateFileDbSQLite()
        {
            bool result;

            try
            {
                SQLiteConnection.CreateFile(BootLoader.PathFileDbSBR);

                connection.Open();

                using (SQLiteCommand command = connection.CreateCommand())
                {
                    command.CommandText = "CREATE TABLE Reports ("
                                + "ReportID	INTEGER NOT NULL UNIQUE,"
                                + "Name	TEXT NOT NULL UNIQUE,"
                                + "DirectoryName	TEXT NOT NULL UNIQUE,"
                                + "CreationDate	TEXT NOT NULL,"
                                + "ModifiedDate	TEXT NOT NULL,"
                                + "Md5	TEXT NOT NULL CHECK(length(Md5) <= 32) UNIQUE,"
                                + "Content	BLOB NOT NULL,"
                                + "BackupIsCorrect	INTEGER NOT NULL CHECK(BackupIsCorrect >= 0 AND BackupIsCorrect <= 1),"
                                + "FileSize	INTEGER NOT NULL,"
                                + "PRIMARY KEY(ReportID AUTOINCREMENT));";

                    command.ExecuteNonQuery();

                    command.CommandText = "SELECT COUNT(name) FROM sqlite_master WHERE type = 'table' AND name = 'Reports';";

                    result = (long)command.ExecuteScalar() == 1;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                connection.Close();
            }

            return result;
        }

        public void CheckingConnectionSql()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }


        public TableFileReports TableFileReports { get; private set; }
    }
}
