﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace ScanBackupReports
{
    class TaskManager
    {
        private List<string> reportFilePaths;
        private List<FileReport> reports;

        public TaskManager()
        {
            reportFilePaths = null;

            reports = new List<FileReport>();
        }

        public void LoadReportFiles()
        {
            ClearingAllCollections();

            string scanDirectory = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_PATH_SCANS_FILES);
            string nameFileToSearchFor = BootLoader.GetStringValueOfSettingsFile(KeysFileSettings.KEY_TEMPLATE_SCANS_FILES);

            reportFilePaths = Directory.EnumerateFiles(scanDirectory).Where(item => item.EndsWith(".txt") && item.Contains(nameFileToSearchFor)).ToList();

            if (reportFilePaths.Count == 0)
            {
                Helper.HubForReports = null;
                return;
            }

            for (int i = 0; i < reportFilePaths.Count; i++)
            {
                FileInfo fileInfo = new FileInfo(reportFilePaths[i]);

                string name = fileInfo.Name;
                string fullName = fileInfo.FullName;
                long size = fileInfo.Length;
                DateTime dateCreation = fileInfo.CreationTime;
                DateTime dateModified = fileInfo.LastWriteTime;

                using (FileStream stream = fileInfo.OpenRead())
                {
                    byte[] content = new byte[stream.Length];

                    stream.Read(content, 0, content.Length);

                    using (MD5 hash = MD5.Create())
                    {
                        string hashMD5 = BitConverter.ToString(hash.ComputeHash(content)).Replace("-", "").ToLowerInvariant();

                        bool backupIsCorrect = CheckingTheBackup(content);

                        reports.Add(new FileReport(name, fullName, hashMD5, size, dateCreation, dateModified, content, backupIsCorrect));
                    }
                }
            }

            Helper.HubForReports = reports;
        }

        public string ScanResult()
        {
            int successfulReports = reports.FindAll(item => item.BackupIsCorrect == true).Count;
            int totalReports = reports.Count;

            DateTime lastReport = reports.Max(item => item.DateModified);
            DateTime firstReport = reports.Min(item => item.DateModified);

            return "\nВсего отчётов: " + totalReports + "\n\n"
                    + "Успешных: " + successfulReports + "\n"
                    + "Не успешных: " + (totalReports - successfulReports) + "\n\n"
                    + "Последний отчёт: " + lastReport + "\n\n"
                    + "Первый отчёт: " + firstReport;
        }

        private bool CheckingTheBackup(byte[] contentFile)
        {
            string[] content = Encoding.UTF8.GetString(contentFile).Split(new string[] { "\r\n" }, StringSplitOptions.None);

            string successfully = "Успешно";
            int rowBackup = BootLoader.GetIntValueOfSettingsFile(KeysFileSettings.KEY_BACKUP_SCANS_FILES) - 1;
            int rowIntegrity = BootLoader.GetIntValueOfSettingsFile(KeysFileSettings.KEY_INTEGRITY_SCANS_FILES) - 1;

            if (rowBackup < content.Length && rowIntegrity < content.Length)
            {
                if (content[rowBackup] == successfully && content[rowIntegrity] == successfully)
                {
                    return true;
                }
            }
            
            #region 1
            /*int count = content.Count;

            if (checkingRowBackup != null && checkingRowIntegrity != null && checkingRowBackup < count && checkingRowIntegrity < count)
            {
                int rowBackup = checkingRowBackup ?? default(int);
                int rowIntegrity = checkingRowIntegrity ?? default(int);

                if (content[rowBackup] == successfully && content[rowIntegrity] == successfully)
                {
                    return true;
                }

            return false;*/
            #endregion

            return false;
        }

        private void ClearingAllCollections()
        {
            reports.Clear();
        }


        public int CountReports
        {
            get { return reports.Count; }
        }

    }
}
